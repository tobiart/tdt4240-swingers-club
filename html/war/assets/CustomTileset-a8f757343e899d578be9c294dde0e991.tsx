<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="CustomTileset" tilewidth="16" tileheight="16" tilecount="16" columns="4">
 <image source="../CustomTileset.png" trans="fdf4ff" width="64" height="64"/>
 <tile id="0">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0" y="4">
    <polygon points="0,0 7,0 12,5 12,12 16,12 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index" id="3">
   <object id="6" x="0" y="0" width="4" height="16"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="4" height="4"/>
   <object id="2" x="12" y="0" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="12" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index" id="3">
   <object id="4" x="4" y="0">
    <polygon points="0,0 0,7 5,12 12,12 12,16 -4,16 -4,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="0" width="4" height="16"/>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="16">
    <polygon points="0,0 0,-7 5,-12 12,-12 12,-16 -4,-16 -4,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="4" height="4"/>
   <object id="2" x="12" y="12" width="4" height="4"/>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12">
    <polygon points="0,0 7,0 12,-5 12,-12 16,-12 16,4 0,4"/>
   </object>
  </objectgroup>
 </tile>
</tileset>

# TDT4240 - Swingers club


To run the game, you need to ensure you have Android SDK 11.0 or higher and an Android device or emulator installed on your system. A popular IDE to run the game from is Android Studio.

1. **Download the project** on your local computer. It can be found on https://gitlab.stud.idi.ntnu.no/tobiart/tdt4240-swingers-club. <br>Open your Terminal and write "git clone https://gitlab.stud.idi.ntnu.no/tobiart/tdt4240-swingers-club.git".

2. **Open Android Studio.** Click Open → tdt4240-swingers-club.

3. **Edit local.properties** to contain the path to the SDK on your device.
    > sdk.dir=C\:\\Users\\USER NAME\\AppData\\Local\\Android\\Sdk for Windows

    > sdk.dir=/Users/USER NAME/Library/Android/sdk for Mac
  
4. **Make an Android emulator.** Go to View → Tool Windows → Device Manager. Under the Virtual-tab, click on "Create Device" (1). Choose whichever phone and system image you want, but the startup orientation should be "Landscape". A new device should show up in the Device Manager-window.

5. **Run the Android emulator** by clicking the Play button, either beside the device you made (2) or by the toolbar (3). A new window running the game should pop up.

<img src="./assets/user-manual-emulator.png" alt="Android emulator in Android Studio" style="width:100%;"/>

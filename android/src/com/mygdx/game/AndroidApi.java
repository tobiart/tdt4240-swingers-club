package com.mygdx.game;


import static android.content.ContentValues.TAG;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mygdx.game.model.Player;
import com.mygdx.game.model.Scoreboard;
import com.mygdx.game.view.WaitingRoomView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class AndroidApi implements API {
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://tdt4240-swingers-club-default-rtdb.europe-west1.firebasedatabase.app/");
    DatabaseReference myRef = database.getReference();

    @Override
    public void submitHighscore(Scoreboard newHighscores) {
        myRef.child("highscores").removeValue();
        for (HashMap.Entry<String, ArrayList<Score>> entry : newHighscores.getScoreListMap().entrySet()) {
            for (Score score : entry.getValue()){
                System.out.println("Entry key:"+entry.getKey()+", Score name: "+score.getPlayer()+", Score: "+score.getScore());
                myRef.child("highscores").child(entry.getKey()).child(score.getPlayer()).setValue(score.getScore());
            }
        }

    }

    @Override
    public void getHighscores(Scoreboard dataHolder){
        System.out.println("Getting highscores");
        dataHolder.setFetching(true);
        myRef.child("highscores").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                System.out.println("Got highscores");
                Iterable<DataSnapshot> response = task.getResult().getChildren();
                for (DataSnapshot child : response) {
                    //System.out.println(child.getKey() + ": " + child.getValue());
                    HashMap<String, Object> childMap = (HashMap<String, Object>) child.getValue();
                    for (HashMap.Entry<String, Object> entry : childMap.entrySet()) {
                        dataHolder.addScore(child.getKey(), new Score(((Long) entry.getValue()).intValue(), entry.getKey()));

                    }
                }
                dataHolder.setFetching(false);
            }
        });
    }

    @Override
    public void getScores(int sessionId, Scoreboard dataHolder){
        myRef.child("sessions").child(Integer.toString(sessionId)).child("Scores").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                Iterable<DataSnapshot> response = task.getResult().getChildren();

                for (DataSnapshot child : response) {
                    System.out.println(child.getKey() + ": " + child.getValue());

                    HashMap<String, Object> childMap = (HashMap<String, Object>) child.getValue();
                    for (HashMap.Entry<String, Object> entry : childMap.entrySet()) {
                        if (!entry.getKey().equals("score")){
                            dataHolder.addScore(entry.getKey(), new Score(((Long) entry.getValue()).intValue(), child.getKey()));
                        }
                    }
                }
            }
        });
    }


    @Override
    public void hitGoal(int sessionId, Player player, int courseNr) {
        System.out.println("Inni API hitGoal, Player: "+player.getPlayerName()+", their strokes: "+player.getStrokes());
        myRef.child("sessions").child(String.valueOf(sessionId)).child("CurrentlyFinished").child(player.getPlayerName()).setValue(true);
        myRef.child("sessions").child(String.valueOf(sessionId)).child("Scores").child(player.getPlayerName()).child("course"+(courseNr+1)).setValue(player.getStrokes());
    }
    boolean hasListener = false;
    @Override
    public void watchBalls(int sessionId,ArrayList<Player> playerList, boolean addListener) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (Player player:
                        playerList) {
                    player.getBall().getBody().setTransform((float) dataSnapshot.child(player.getPlayerName()).child("x").getValue(double.class).doubleValue(), (float) dataSnapshot.child(player.getPlayerName()).child("y").getValue(double.class).doubleValue(), 0.0F);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        if(addListener){
            if(!hasListener){
                //      System.out.println("Legger til session");
                myRef.child("sessions").child(sessionId+"").child("Balls").addValueEventListener(postListener);
                hasListener = !hasListener;

            }
        }else{
            if(hasListener){
                //    System.out.println("Fjerner session");
                myRef.child("sessions").child(sessionId+"").child("Balls").removeEventListener(postListener);
                hasListener = !hasListener;

            }
        }
    }

    @Override
    public void sendBallData(int sessionId, ArrayList<Player> players) {
        for (int i = 0; i < players.size(); i = i + 1) {
            try {
                myRef.child("sessions").child(String.valueOf(sessionId)).child("Balls").child(players.get(i).getPlayerName()).setValue(players.get(i).getBall().getBody().getPosition());
            }
            catch(Exception e) {
                System.out.println(e);
            }
        }

    }

    @Override
    public void createSession(int sessionId, String player) {
        myRef.child("sessions").child("" + sessionId).child("Scores").child(player).child("score").setValue(0);
        myRef.child("sessions").child("" + sessionId).child("Players").child(player).setValue(0);
        myRef.child("sessions").child(String.valueOf(sessionId)).child("CurrentlyFinished").child(player).setValue(false);
        myRef.child("sessions").child("" + sessionId).child("Balls").child(player).child("x").setValue(0);
        myRef.child("sessions").child("" + sessionId).child("Balls").child(player).child("y").setValue(0);
        myRef.child("sessions").child("" + sessionId).child("Started").setValue(false);
        myRef.child("sessions").child("" + sessionId).child("CurrentPlayer").setValue(0);
        myRef.child("sessions").child("" + sessionId).child("CurrentCourse").setValue(0);


    }

    @Override
    public boolean joinSession(int sessionId, String player, Golf golf) {
        final boolean[] returnValue = new boolean[1];
        myRef.child("sessions").child("" + sessionId).child("Started").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                System.out.println(task.getResult().getValue());
                if(!task.getResult().getValue(boolean.class)){
                    myRef.child("sessions").child("" + sessionId).child("Scores").child(player).child("score").setValue(0);
                    myRef.child("sessions").child(String.valueOf(sessionId)).child("CurrentlyFinished").child(player).setValue(false);
                    myRef.child("sessions").child("" + sessionId).child("Players").child(player).setValue(0);
                    myRef.child("sessions").child("" + sessionId).child("Balls").child(player).child("x").setValue(0);
                    myRef.child("sessions").child("" + sessionId).child("Balls").child(player).child("y").setValue(0);
                    golf.changeToWaitingRoom(sessionId, player, false);
                }
                else{
                    golf.changeToErrorFeedback();
                }

            }
        });
        return returnValue[0];
    }

    @Override
    public void unsetFinished(int sessionId, String player) {
        myRef.child("sessions").child(String.valueOf(sessionId)).child("CurrentlyFinished").child(player).setValue(false);
    }


    @Override
    public void isFinishedListener(int sessionId, ArrayList<Player> playerFinishedStatus) {
        System.out.println("Setter listener");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (Player player:
                        playerFinishedStatus) {
                    //  System.out.println("ReturnData:"+dataSnapshot.child(player.getPlayerName()).getValue());
                    player.setFinishedCourse(dataSnapshot.child(player.getPlayerName()).getValue(boolean.class));

                }
                /*for (Object el : iterable){
                    playerFinishedStatus.add((Boolean) el.chi);
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        myRef.child("sessions").child(sessionId+"").child("CurrentlyFinished").addValueEventListener(postListener);

    }

    @Override
    public void startSession(int sessionId) {
        myRef.child("sessions").child("" + sessionId).child("Started").setValue(true);
    }

    @Override
    public void getPlayers(int sessionId, ArrayList<Player> dataHolder) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("Nå henter den playerdata");
                dataHolder.clear();
                Iterable<DataSnapshot> response = dataSnapshot.getChildren();
                int count = 0;
                for (DataSnapshot child : response) {
                    Player player = new Player(count, child.getKey());
                    count++;
                    dataHolder.add(player);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        myRef.child("sessions").child(sessionId+"").child("Players").addValueEventListener(postListener);
    }

    @Override
    public void waitForGameStart(int sessionId, ArrayList<Boolean> dataHolder, WaitingRoomView waitingRoomView) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Boolean> dataHolder = new ArrayList<>();
                dataHolder.add((Boolean) dataSnapshot.getValue());
                waitingRoomView.addListener(dataHolder.get(0));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };

        myRef.child("sessions").child(sessionId+"").child("Started").addValueEventListener(postListener);
    }

    @Override
    public void addStroke(int sessionId, Player player) {
        //  myRef.child("sessions").child("" + sessionId).child("Players").child(player.getPlayerName()).child("strokes").setValue(player.getStrokes()+1);

    }

    @Override
    public void setCurrentPlayer(int sessionId, int player) {
        ///   System.out.println("Seeting new currentplayer:"+player);
        myRef.child("sessions").child("" + sessionId).child("CurrentPlayer").setValue(player);
    }

    @Override
    public void currentPlayerListener(int sessionId, ArrayList<Integer> dataHolder) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //           System.out.println("Currentplayer:"+dataSnapshot.getValue(Integer.class));
                dataHolder.set(0,(dataSnapshot.getValue(Integer.class)));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };

        myRef.child("sessions").child("" + sessionId).child("CurrentPlayer").addValueEventListener(postListener);

    }

    @Override
    public void currentCourseListener(int sessionId, ArrayList<Integer> dataHolder) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //           System.out.println("Currentplayer:"+dataSnapshot.getValue(Integer.class));
                dataHolder.set(0,(dataSnapshot.getValue(Integer.class)));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };

        myRef.child("sessions").child("" + sessionId).child("CurrentCourse").addValueEventListener(postListener);

    }

    @Override
    public void setCurrentCourse(int sessionId, int course) {
        myRef.child("sessions").child("" + sessionId).child("CurrentCourse").setValue(course+1);
    }

    @Override
    public void sendTeleportedBall(int sessionId, Player player) {
        myRef.child("sessions").child(String.valueOf(sessionId)).child("Balls").child(player.getPlayerName()).setValue(player.getBall().getBody().getPosition());

    }


}

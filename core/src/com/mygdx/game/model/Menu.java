package com.mygdx.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.API;

public class Menu {
    private static Menu INSTANCE;

    private static final float WORLD_WIDTH = 200f; // set the width of the world in meters
    private static final float WORLD_HEIGHT = 180f; // set the height of the world in meters


    private World world;
    private Box2DDebugRenderer debugRenderer;
    private OrthographicCamera camera;
    static API api;

    private Menu(API api) {
        world = new World(new Vector2(0, 0), true);
        debugRenderer = new Box2DDebugRenderer();
        float aspectRatio = (float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth();
        camera = new OrthographicCamera(WORLD_WIDTH, WORLD_HEIGHT * aspectRatio);
        float hx = WORLD_WIDTH / 2f;
        float hy = WORLD_HEIGHT * aspectRatio / 2f;

        camera.position.set(115, 210, 0); // center the camera
        camera.update();
        this.api = api;

    }
    public static Menu getInstance(API api){
        if(INSTANCE == null) {
            INSTANCE = new Menu(api);
        }
        return INSTANCE;
    }
    public void update(){
        world.step(1/60f, 6, 2);

    }
    public OrthographicCamera getCamera(){
        return camera;
    }
}

package com.mygdx.game.model;

import com.badlogic.gdx.math.Vector2;

public class Goal {

    private Float x;
    private Float y;

    private Float rad;

    public Goal(Float x, Float y, Float rad){
        this.x = x;
        this.y = y;
        this.rad = rad;
    }
    public Vector2 getPos() {
        return new Vector2(x, y);
    }

    public Float getRad() {
        return rad;
    }
}

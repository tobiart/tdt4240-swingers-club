package com.mygdx.game.model;

import com.mygdx.game.API;
import com.mygdx.game.Score;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Scoreboard {
    private int scoreLength;
    private ArrayList<Score> course1;
    private ArrayList<Score> course2;
    private ArrayList<Score> course3;
    private ArrayList<Score> course4;
    private ArrayList<Score> totalScore;
    private Comparator<Score> nameComparator;
    private boolean isFetching = false;
    private boolean needUpdate;
    
    public Scoreboard(){
        course1 = new ArrayList<>();
        course2 = new ArrayList<>();
        course3 = new ArrayList<>();
        course4 = new ArrayList<>();
        totalScore = new ArrayList<>();
        scoreLength = 0;

        nameComparator = new Comparator<Score>() {
            @Override
            public int compare(Score o1, Score o2) {
                return o1.getPlayer().compareTo(o2.getPlayer());
            }
        };
    }

    public ArrayList<Score> getScoreList(String scoreList){
        if (scoreList.equals("course1")){
            return course1;
        } else if (scoreList.equals("course2")) {
            return course2;
        } else if (scoreList.equals("course3")) {
            return course3;
        } else if (scoreList.equals("course4")) {
            return course4;
        } else if (scoreList.equals("total")) {
            return totalScore;
        } else {
            System.out.println("Error: Invalid list");
            return null;
        }
    }
    
    public void setScorelist(String scoreList, ArrayList<Score> newList){
        scoreLength = newList.size();
        if (scoreList.equals("course1")){
            course1 = newList;
        } else if (scoreList.equals("course2")) {
            course2 = newList;
        } else if (scoreList.equals("course3")) {
            course3 = newList;
        } else if (scoreList.equals("course4")) {
            course4 = newList;
        } else if (scoreList.equals("total")) {
            totalScore = newList;
        } else {
            System.out.println("Error: Invalid list");
        }
    }

    public void addScore(String scoreList, Score newScore){
        if (scoreList.equals("course1")){
            course1.add(newScore);
            Collections.sort(course1);
        } else if (scoreList.equals("course2")) {
            course2.add(newScore);
            Collections.sort(course2);
        } else if (scoreList.equals("course3")) {
            course3.add(newScore);
            Collections.sort(course3);
        } else if (scoreList.equals("course4")) {
            course4.add(newScore);
            Collections.sort(course4);
        } else if (scoreList.equals("total")) {
            totalScore.add(newScore);
            Collections.sort(totalScore);
        } else {
            System.out.println("Error: Invalid list");
        }
        scoreLength = course1.size();
    }

    public int getScoreLength(){
        return scoreLength;
    }

    public void setFetching(boolean setting){
        isFetching = setting;
    }
    
    public void calculateTotal(){
        for (int i = 0; i < scoreLength; i++){
            System.out.println("Scorelength: "+scoreLength+", i: "+i);
            int total = 0;
            try {
                total += course1.get(i).getScore();
            } catch (IndexOutOfBoundsException e){
                total += 0;
            } try {
                total += course2.get(i).getScore();
            } catch (IndexOutOfBoundsException e){
                total += 0;
            } try {
                total += course3.get(i).getScore();
            } catch (IndexOutOfBoundsException e){
                total += 0;
            }try {
                total += course4.get(i).getScore();
            } catch (IndexOutOfBoundsException e){
                total += 0;
            }
            totalScore.add(new Score(total, course1.get(i).getPlayer()));
        }
        Collections.sort(totalScore);
    }

    public void sortByName(){
        Collections.sort(course1, nameComparator);
        Collections.sort(course2, nameComparator);
        Collections.sort(course3, nameComparator);
        Collections.sort(course4, nameComparator);
        Collections.sort(totalScore, nameComparator);
    }

    public HashMap<String, ArrayList<Score>> getScoreListMap(){
        HashMap<String, ArrayList<Score>> scorelistMap = new HashMap<>();
        scorelistMap.put("course1", course1);
        scorelistMap.put("course2", course2);
        scorelistMap.put("course3", course3);
        scorelistMap.put("course4", course4);
        scorelistMap.put("total", totalScore);
        return scorelistMap;
    }

    public String toString(){
        String output = "";
        output += "course1: {";
        for (Score score : course1){
            output += score.toString() + ", ";
        }
        output += "}, \ncourse2: {";
        for (Score score : course2){
            output += score.toString() + ", ";
        }
        output += "}, \ncourse3: {";
        for (Score score : course3){
            output += score.toString() + ", ";
        }
        output += "}, \ncourse4: {";
        for (Score score : course4){
            output += score.toString() + ", ";
        }
        output += "}, \ntotal: {";
        for (Score score : totalScore){
            output += score.toString() + ", ";
        }
        output += "}.\n";

        return output;
    }

    public void updateDatabaseHighscore(API api){
        Scoreboard highscores = new Scoreboard();
        api.getHighscores(highscores);
        while (highscores.isFetching){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } // Wait for fetching to finish
        System.out.println(highscores);
        needUpdate = false;
        compareAndUpdateHighscores(this.course1, highscores.getScoreList("course1"));
        compareAndUpdateHighscores(this.course2, highscores.getScoreList("course2"));
        compareAndUpdateHighscores(this.course3, highscores.getScoreList("course3"));
        compareAndUpdateHighscores(this.course4, highscores.getScoreList("course4"));
        compareAndUpdateHighscores(this.totalScore, highscores.getScoreList("total"));

        System.out.println("Er needUpdate true? "+ needUpdate);
        if (needUpdate){
            api.submitHighscore(highscores);
        }
    }

    public void compareAndUpdateHighscores(ArrayList<Score> newScoreList, ArrayList<Score> highscoreList){
        for (Score score : newScoreList) {
            if (highscoreList.size() < 5 || score.getScore() <= highscoreList.get(highscoreList.size() - 1).getScore()) {
                needUpdate = true;
                System.out.println("needUpdate settes til true");

                int i = 0;
                while (i < highscoreList.size() && score.getScore() > highscoreList.get(i).getScore()) {
                    i++;
                }
                highscoreList.add(i, score);
                Collections.sort(highscoreList);
                while (highscoreList.size() > 5) {
                    highscoreList.remove(highscoreList.size()-1);
                }
            }
        }
    }
    

    public void clear(){
        course1.clear();
        course2.clear();
        course3.clear();
        course4.clear();
        totalScore.clear();
        scoreLength = 0;
    }
    
    
    
}

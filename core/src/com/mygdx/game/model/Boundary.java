package com.mygdx.game.model;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Boundary {
    private Body boundaryBody;

    public Boundary(World world, float x, float y, float width, float height) {
        // create the boundary body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x + width / 2f, y + height / 2f);
        boundaryBody = world.createBody(bodyDef);

        // create the boundary shape
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width / 2f, height / 2f);

        // create the fixture
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.restitution = 1f; // set the restitution to make the ball bounce
        boundaryBody.createFixture(fixtureDef);

        // clean up
        shape.dispose();
    }
}

package com.mygdx.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.FrictionJointDef;

import jdk.internal.net.http.common.Log;

public class Ball {

    private Body body;
    private World gameWorld;
    private Float rad;
    private boolean removed;

    public Ball(World world, int x, int y){
        removed = false;
        gameWorld = world;
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.linearDamping = 1f;
        bodyDef.angularDamping = 0.5f;
        body = gameWorld.createBody(bodyDef);

        CircleShape shape = new CircleShape();
        rad = 1f;
        shape.setRadius(rad);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.01f;
        fixtureDef.restitution = 0.6f;




        body.createFixture(fixtureDef);
        shape.dispose();
        // fShape.dispose();
    }

    public void setActive(boolean setting){
        //Virker annerledes fra destroy i at man kan fjerne og adde samme objekt igjen.
        getBody().setActive(setting);
    }
    public void destroy(){
        //Fjerner en body (ballen assa) fra gameworld permanent
        Gdx.app.log("MyTag", "Ball removed");
        removed = true;
        gameWorld.destroyBody(body);
    }

    public Body getBody() {
        return body;
    }

    public Float getRad() {
        return rad;
    }

    
}

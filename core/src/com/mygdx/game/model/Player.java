package com.mygdx.game.model;

import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.API;

public class Player {

    private Ball ball;
    private Integer score;
    private int playerIndex;

    public int getStrokes() {
        return strokes;
    }

    public void addStroke() {
        strokes++;
    }
    public void clearStrokes(){strokes = 0;}

    private int strokes;
    private String name;

    private boolean finishedCourse;

    public Player(int index, String name){
        score = 0;
        strokes = 0;
        playerIndex = index;
        finishedCourse = false;
        this.name = name;
    }

    public void initBall(World world, int x, int y){
        ball = new Ball(world, x, y);
    }

    public Ball getBall() {
        return ball;
    }

    public boolean getFinishedCourse() { return finishedCourse; }

    public void setFinishedCourse(boolean finishedCourse) { this.finishedCourse = finishedCourse;}


    public String getPlayerName() {
        return name;
    }
}

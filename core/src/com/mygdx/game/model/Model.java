package com.mygdx.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.view.View;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import javax.swing.plaf.synth.SynthEditorPaneUI;

public class Model implements ContactListener {

    private static Model INSTANCE;

    private static final float WORLD_WIDTH = 200f; // set the width of the world in meters
	private static final float WORLD_HEIGHT = 180f; // set the height of the world in meters

	private World world;
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;
    private Goal[] goals;
	private int sendDataCounter;
	private ArrayList<Integer> ballStartPosX,ballStartPosY;
	private Boundary[] boundaries;
	private ArrayList<Integer> currentPlayer;
	private Vector3 tp = new Vector3();
	private String player;

	public void setPlayerFinishedStatusListener(){
		api.isFinishedListener(sessionId,playerList);
	}

	public ArrayList<Player> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(ArrayList<Player> playerList) {
		this.playerList = playerList;
	}

	private ArrayList<Player> playerList;

	//private int currentPlayer = 0;
	boolean waitForBall;

	public boolean isCourseOver() {
		return courseOver;
	}
	public void setCourseNotOver(){
		for (int x = 0; x < playerList.size(); x++){
			playerList.get(x).setFinishedCourse(false);
			if (isActivePlayer()){
				api.unsetFinished(sessionId, playerList.get(x).getPlayerName());
			}
		}
		courseOver = false;
	}
	boolean courseOver;
	boolean gameOver;

	private int course = 0;

	static API api;

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getSessionId() {
		return sessionId;
	}

	private int sessionId;

    private Model(API api){
		sendDataCounter=0;
        world = new World(new Vector2(0, 0), true);
		debugRenderer = new Box2DDebugRenderer();
		float aspectRatio = (float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth();
		camera = new OrthographicCamera(WORLD_WIDTH, WORLD_HEIGHT * aspectRatio);
		float hx = WORLD_WIDTH / 2f;
		float hy = WORLD_HEIGHT * aspectRatio / 2f;

		camera.position.set(0, 0, 0); // center the camera
		camera.update();

		Courses.parseMap(world, Courses.course);

		//createBall();
		this.api = api;
		//currentPlayer = new ArrayList<>();
		//currentPlayer.add(0);

		//Init goals
        goals = new Goal[4];
        goals[0] = new Goal(135f, 25f, 2f);
        goals[1] = new Goal(23f, 169f, 2f); //burde hentes fra course
		goals[2] = new Goal(23f, 360f, 2f); //burde hentes fra course
		goals[3] = new Goal(23f, 730f, 2f); //burde hentes fra course

		//Init ball starting positions
		ballStartPosX = new ArrayList<>();
		ballStartPosX.add(20);
		ballStartPosX.add(20);
		ballStartPosX.add(20);
		ballStartPosY = new ArrayList<>();
		ballStartPosY.add(220);
		ballStartPosY.add(410);
		ballStartPosY.add(680);


		waitForBall = false;
		gameOver = false;

		world.setContactListener(this);
    }

    public static Model getInstance(API api){
        if(INSTANCE == null) {
            INSTANCE = new Model(api);
        }
        return INSTANCE;
    }

	public void startNewGame(int sessionId, String player){
		course = 0;
		waitForBall = false;
		System.out.println("Setter gameOver til false");
		gameOver = false;
		courseOver = false;

		currentPlayer = new ArrayList<>();
		currentPlayer.add(0);

		this.sessionId = sessionId;
		System.out.println("SessionId = "+ sessionId);
		setCurrentPlayerListener();
		setPlayer(player);

		initPlayerBalls();
		setPlayerFinishedStatusListener();


	}

	public boolean isActivePlayer(){
		if(player.equals(playerList.get(currentPlayer.get(0)).getPlayerName())){
			return true;
		}else{
			return false;
		}
	}

	public boolean isGameOver(){
		return gameOver;
	}

	/**
	 * Updates the game world and makes the necessary logic checks that needs to be
	 * done at each step, as long as the course isn't over.
	 */
    public void update(){
	//	gameOver = false;
		if (gameOver){
			System.out.println("Her i start av update. gameOver = true");
			return;
		}
		if (!courseOver) {
			if (allPlayersFinished()){
				System.out.println("Course finished");
				courseOver = true;
				if (course > ballStartPosX.size()){
					gameOver = true;
					return;
				}
			}


			// Update the world
			world.step(1 / 60f, 6, 2);
			handleBallInGoal();
			if(isActivePlayer()){
				upDateBalls(false);
				if(sendDataCounter==1) {
					api.sendBallData(sessionId, playerList);
					sendDataCounter=0;
				}
				sendDataCounter++;
			}
			else {
				upDateBalls(true);
			}
		}

		if (waitForBall) {
			if (!checkBallMoving()) {
				waitForBall = false;
				nextPlayer();
			}
		}

	}

	/**
	 * Moves to the next active player, and sets 'courseOver' if
	 * all players are finished with the course.
	 * @return
	 */
	private void nextPlayer(){
		currentPlayer.set(0, (currentPlayer.get(0) + 1) % playerList.size());
		int c = 0;
		for (int x = 0; x < playerList.size(); x++){
			if (playerList.get(currentPlayer.get(0)).getFinishedCourse()) {
				currentPlayer.set(0, (currentPlayer.get(0) + 1) % playerList.size());
				c++;
			}
		}
		if (c == playerList.size()){
			currentPlayer.set(0, (currentPlayer.get(0) + 1) % playerList.size());
		}
		setCurrentPlayer(currentPlayer.get(0));
		
		if (!allPlayersFinished()){
			return;
		}

		if (course > ballStartPosX.size()){
			System.out.println("Say gameOver"+player);
			gameOver = true;
			return;
		}

		courseOver = true;
		System.out.println("NEXT PLAYER COURSE FINISHED");

		//Course over

		return;


	}
	public boolean allPlayersFinished(){
		for (int i=0;i<playerList.size();i++){
			if(!playerList.get(i).getFinishedCourse()){
//				System.out.println(playerList.get(i).getPlayerName()+ "Has not finished");
				//	System.out.println(playerList.get(i).getFinishedCourse());
				return false;
			}
		}
		return true;
	}

	private void upDateBalls(boolean addListener) {
		api.watchBalls(sessionId,playerList, addListener);
	}
	/**
	 * Checks whether any balls are within the range of any goal,
	 * removes such balls, sets the player to 'finished course',
	 * and moves to the next player.
	 */
    private void handleBallInGoal(){
		for(int i = 0; i < playerList.size(); i++){
			for (int j = 0; j < goals.length; j++){
				if (!playerList.get(i).getFinishedCourse() && playerList.get(i).getBall() != null) {
					if (checkBallInGoal(playerList.get(i).getBall(), goals[j])) {
						System.out.println("Player index i: "+ i + ", goal index j: " + j);
						if (course < ballStartPosX.size()){
							playerList.get(i).getBall().getBody().setTransform(ballStartPosX.get(course)+i*5, ballStartPosY.get(course)+i*5, 0);
							api.sendTeleportedBall(sessionId,playerList.get(i));
							System.out.println(playerList.get(i).getBall().getBody().getPosition());

							playerList.get(i).getBall().getBody().setLinearVelocity(0,0);
						}
						if (course > 0){
							System.out.println("Ball in goal done!");
						}
						System.out.println("Finished course nr: " + course + ", setting player finished course");
						playerList.get(i).setFinishedCourse(true);

						System.out.println("Da er vi her");
						api.hitGoal(sessionId, playerList.get(i), course);
						playerList.get(i).clearStrokes();
						course++;
						nextPlayer();
						break;
					}
				}
			}
		}

    }
	public void initPlayerBalls(){
		for (int i = 0; i < playerList.size(); i++) {
			playerList.get(i).initBall(world,  i*2 + 20, i*2 + 25);
		}
	}

	/**
	 * Checks if the current players ball is in motion.
	 */
	private boolean checkBallMoving(){
		try {
			return (playerList.get(currentPlayer.get(0)).getBall().getBody().getLinearVelocity().len2() >= 1);
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	/**
	 * Checks if the given ball is within the range of a given goal.
	 */
    private boolean checkBallInGoal(Ball ball, Goal goal){
        return (goal.getRad() - ball.getRad()) > ball.getBody().getPosition().dst(goal.getPos());
    }

    public Goal[] getGoals(){
        return goals;
    }

    public Vector2 getBallPos(int index){
		try{
			return playerList.get(index).getBall().getBody().getPosition();
		} catch (Exception e){
			System.out.println(e);
		}
		return new Vector2(0, 0);
	}

	public float getBallRad(int index){
		try {
			return playerList.get(index).getBall().getRad();
		} catch (Exception e){
			System.out.println(e);
		}
		return 0;
	}

    public int getCurrentPlayer(){
        return currentPlayer.get(0);
    }


	/**
	 * Gets a list of players who currently hasn't finished the course.
	 */
	public List<Integer> getActivePlayers(){
		List<Integer> activePlayers = new ArrayList<>();
		for (Player player : playerList){
			if (!player.getFinishedCourse()){
				activePlayers.add(playerList.indexOf(player));
			}
		}
		return activePlayers;
	}

	public Vector3 getTp(){
		return tp;
	}

	public OrthographicCamera getCamera(){
		return camera;
	}

	public boolean getWaitForBall(){
		return waitForBall;
	}

    public void ballTouchDown(int screenX, int screenY){
		if (!waitForBall){
			camera.unproject(tp.set(screenX, screenY, 0));
		}

	}

	public void ballTouchDragged(int screenX, int screenY){
		if (!waitForBall){
			camera.unproject(tp.set(screenX, screenY, 0));
		}
	}

	public void ballTouchUp(int screenX, int screenY){
		if (!waitForBall){
			camera.unproject(tp.set(screenX, screenY, 0));
			Vector2 force = playerList.get(currentPlayer.get(0)).getBall().getBody().getPosition().sub(new Vector2(tp.x, tp.y));
			playerList.get(currentPlayer.get(0)).getBall().getBody().applyForceToCenter(force.scl(500), true);
			playerList.get(currentPlayer.get(0)).addStroke();
			waitForBall = true;
		}
	}

	@Override
	public void beginContact(Contact contact) {
		Object objA = contact.getFixtureA().getBody().getUserData();
		Object objB = contact.getFixtureB().getBody().getUserData();
	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

	public void dispose(){
		world.dispose();
		debugRenderer.dispose();
	}

	public int getCourse() {
		return course;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public void setCurrentPlayer(int player) {
		api.setCurrentPlayer(sessionId,player);
	}
	public void setCurrentPlayerListener(){
		api.currentPlayerListener(sessionId,currentPlayer);
	}
}

package com.mygdx.game.model.dbModels;

public class Session {
    public int sessionId;
    public Player player;

    public Session(int sessionId, String userName) {
        this.sessionId = sessionId;
        this.player= new Player(userName);
    }

}

package com.mygdx.game.model.dbModels;

public class Player {
    public String name;
    public int score;


    public Player(String name) {
        this.name = name;
        this.score = 0;
    }

}

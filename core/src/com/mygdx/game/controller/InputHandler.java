package com.mygdx.game.controller;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.model.Model;
import com.mygdx.game.view.View;

public class InputHandler extends InputAdapter {

    private static InputHandler INSTANCE;

    private boolean dragging;

    View view;
    Model model;

    private InputHandler (API api, View view){
        this.view = view;
        this.model = Model.getInstance(api);
    }

    public static InputHandler getInstance(API api, View view){
        if(INSTANCE == null){
            INSTANCE = new InputHandler(api, view);
        }
        return INSTANCE;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    @Override public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        // ignore if its not left mouse button or first touch pointer
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        model.ballTouchDown(screenX, screenY);
        dragging = true;
        view.setDragging(true);
        return true;
    }

    @Override public boolean touchDragged (int screenX, int screenY, int pointer) {
        if (!dragging) return false;
        model.ballTouchDragged(screenX, screenY);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }

    @Override public boolean touchUp (int screenX, int screenY, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        if (!dragging){
            return false;
        }
        model.ballTouchUp(screenX, screenY);
        dragging = false;
        view.setDragging(false);
        return true;
    }
}

package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.controller.InputHandler;
import com.mygdx.game.model.Model;
import com.mygdx.game.view.ErrorFeedBackView;
import com.mygdx.game.view.HighscoreView;
import com.mygdx.game.view.JoinGameView;
import com.mygdx.game.view.LeaderboardView;
import com.mygdx.game.view.MenuView;
import com.mygdx.game.view.TutorialView;
import com.mygdx.game.view.View;
import com.mygdx.game.view.WaitingRoomView;

import java.util.ArrayList;

//public class Golf extends ApplicationAdapter implements ContactListener, InputProcessor {
public class Golf extends ApplicationAdapter {
//	private static Golf INSTANCE;

	private Stage stage;
	public String player;
	//private int sessionId;
	private boolean host;


	private String state;

	private WaitingRoomView waitingRoomView;
	private ErrorFeedBackView errorFeedBackView;
	private JoinGameView joinGameView;
	private HighscoreView highscoreView;
	private LeaderboardView leaderboardView;
	private TutorialView tutorialView;
	private int sessionId;
	private Model model;
	private InputHandler input;
	private View view;
	private MenuView menuView;
	private API api;


	public Golf(API androidApi) {
		this.api = androidApi;
    }
	//bare her så android studio ikke klager på at desktop html og ios ikke har noen api
	public Golf() {
	}

	public Golf getGolf(){
		return this;
	}
    @Override
	public void create () {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		//menuView = MenuView.getInstance(api,stage);
		//menuView.createElements(this);
		changeToMenu();
	}

	public void changeToGame(int sessionId, boolean isNewGame) {
		stage.clear();
		model = Model.getInstance(api);

		view = View.getInstance(api);
		input = InputHandler.getInstance(api, view);
		if (isNewGame){
			model.startNewGame(sessionId, player);
		} else {
			System.out.println("SET COURSE NOT OVER");
			model.setCourseNotOver();
		}
		Gdx.input.setInputProcessor(input);
		state = "game";
	//	this.sessionId = sessionId;
		//this.sessionId = sessionId;
	}
	public void changeToErrorFeedback(){
		stage.clear();
		errorFeedBackView = ErrorFeedBackView.getInstance(this, stage);
		state = "error";
	}
	public void changeToWaitingRoom(int sessionId, String player, boolean host) {
		stage.clear();
		waitingRoomView = WaitingRoomView.getInstance(api, stage, this);
		waitingRoomView.init(sessionId);
		ArrayList<Boolean> dataHandler = new ArrayList<>();
		if(!host){
			api.waitForGameStart(sessionId, dataHandler, waitingRoomView);
		}

		state = "lobby";
		this.host=host;
		this.sessionId=sessionId;
		//(//)	this.sessionId=sessionId;
		this.player = player;
	}

	public void changeToMenu(){
		stage.clear();
		state = "menu";
		menuView = MenuView.getInstance(api, stage);
		menuView.createElements(this);
	}

	public void changeToHighscores(){
		stage.clear();
		state = "highscores";
		highscoreView = HighscoreView.getInstance(api, stage, this);
		highscoreView.createElements();
	}

	public void changeToLeaderboard(int sessionId, boolean midgame){
		stage.clear();
		Gdx.input.setInputProcessor(stage);
		state = "leaderboard";
		leaderboardView = LeaderboardView.getInstance(api, stage, this);
		leaderboardView.fetchScores(sessionId);
		leaderboardView.setIsMidgame(midgame);
		leaderboardView.setHighscoreUpdater(model.isActivePlayer());
	}

	public void joinRoom(String player) {
		stage.clear();
		joinGameView = JoinGameView.getInstance(api, stage, this, player);
		joinGameView.createElements();
		this.player = player;
		state = "joining";
	}

	public void goToTutorial() {
		stage.clear();
		state = "tutorial";
		tutorialView = TutorialView.getInstance(stage, this);
	}

	@Override
	public void render () {
		if(state.equals("menu")){
			menuView.render(Gdx.graphics.getDeltaTime());
		} else if (state.equals("lobby")){
			waitingRoomView.setHost(host);
			waitingRoomView.render(Gdx.graphics.getDeltaTime());
		} else if (state.equals("joining")){
			joinGameView.render(Gdx.graphics.getDeltaTime());
		} else if (state.equals("highscores")){
			highscoreView.render(Gdx.graphics.getDeltaTime());
		} else if (state.equals("error")){
			errorFeedBackView.render(Gdx.graphics.getDeltaTime());
		} else if (state.equals("tutorial")) {
			tutorialView.render(Gdx.graphics.getDeltaTime());
		} else if (state.equals("leaderboard")) {
			leaderboardView.render(Gdx.graphics.getDeltaTime());
		}
		else {
			if(model.isActivePlayer()){
				Gdx.input.setInputProcessor(input);
			} else {
				Gdx.input.setInputProcessor(null);
			}
			model.update();
			if (model.isGameOver()) {
				System.out.println("Her i golf, gameOver = true");
				if (model.isActivePlayer()){

				}
				changeToLeaderboard(model.getSessionId(), false);
			} else if (model.isCourseOver()){
				changeToLeaderboard(model.getSessionId(), true);
			} else if (!model.isCourseOver()) {
				view.updateGameView();
			} else {
				System.out.println("ovveveveve");
			}
		}
	}

	public int getSessionId() {
		return sessionId;
	}

	@Override
	public void dispose () {
		menuView.dispose();
		waitingRoomView.dispose();
		joinGameView.dispose();
		view.dispose();
		model.dispose();
		tutorialView.dispose();
	}



}
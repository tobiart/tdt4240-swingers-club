package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.model.Menu;

import java.util.Random;

public class JoinGameView implements Screen {
    private final String userName;
    private final Golf golf;
    private Stage stage;
    private Skin skin;
    private static JoinGameView INSTANCE;
    API api;
    Menu menu;
    private ShapeRenderer shapes;
    private Label outputLabel;
    private TextField sessionIdField;




    public JoinGameView(final API api, Stage stage, final Golf golf, final String userName) {
        this.api = api;
        this.menu = Menu.getInstance(api);
        this.shapes = new ShapeRenderer();
        this.stage = stage;
        this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.5f);
        this.userName = userName;
        this.golf = golf;


    }

    public static JoinGameView getInstance(API api, Stage stage, Golf golf, String player){
        if (INSTANCE == null){
            INSTANCE = new JoinGameView(api, stage, golf, player);
        }
        return INSTANCE;
    }

    public void createElements(){
        sessionIdField = new TextField("SessionId here:", skin);
        sessionIdField.setPosition(Gdx.graphics.getWidth()-(Gdx.graphics.getWidth()/2+100),Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2+300);

        sessionIdField.setSize(200, 50);

        stage.addActor(sessionIdField);
        System.out.println("CREATED");
    }
    @Override
    public void show() {

    }

    @Override
    public void hide() {
        stage = null;
        skin = null;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Button button2 = new TextButton("Join room",skin);
        button2.setSize(100*4,100);
        button2.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2-200,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2-200);
        button2.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                System.out.println();
                api.joinSession(Integer.parseInt(sessionIdField.getText()), userName, golf);
       //         golf.changeToWaitingRoom(Integer.parseInt(sessionIdField.getText()),userName,false);

            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                outputLabel.setText("Pressed Text Button");
                return true;
            }
        });
        stage.addActor(button2);
        Button button = new TextButton("Back to menu", skin);
        button.setSize(100 * 4, 100);
        button.setPosition(Gdx.graphics.getWidth() / 2 - 200, Gdx.graphics.getHeight() / 10);

        button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                golf.changeToMenu();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(button);

        outputLabel = new Label("Enter your friends sessionId!",skin);
        outputLabel.setSize(Gdx.graphics.getWidth(),40);
        outputLabel.setPosition(0,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2+100);
        outputLabel.setAlignment(Align.center);

        stage.addActor(outputLabel);

        stage.act();
        try {
            stage.draw();
        } catch (Exception e) {
            System.out.println("ERROR DRAWING JOIN GAME VIEW");
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        shapes.dispose();
        stage.dispose();


    }}
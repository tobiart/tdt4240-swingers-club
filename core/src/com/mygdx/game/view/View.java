package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.API;
import com.mygdx.game.model.Courses;
import com.mygdx.game.model.Goal;
import com.mygdx.game.model.Model;

import java.util.ArrayList;

public class View {

    private static View INSTANCE;

    private Model model;

    private ShapeRenderer shapes;
    private TiledMapRenderer tiledMapRenderer;
    private boolean dragging;

    private SpriteBatch batch;
    private BitmapFont font;

    private View (API api){
        this.shapes = new ShapeRenderer();
        this.tiledMapRenderer = new OrthogonalTiledMapRenderer(Courses.course);
        this.model = Model.getInstance(api);
        this.font = new BitmapFont();
        this.batch = new SpriteBatch();
    }


    public static View getInstance(API api){
        if (INSTANCE == null){
            INSTANCE = new View(api);
        }
        return INSTANCE;
    }

    public void updateGameView(){
        // Clear the screen
        Gdx.gl.glClearColor(0.25f, 0.44f, 0.21f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw the ball
        //
        OrthographicCamera camera = model.getCamera();
        int currentPlayer = model.getCurrentPlayer();

        Vector2 ballPos = model.getBallPos(currentPlayer);

        Vector3 tp = model.getTp();
        camera.position.lerp(new Vector3(ballPos.x, ballPos.y, 0), Gdx.graphics.getDeltaTime());
        camera.update();

        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();

        shapes.setProjectionMatrix(camera.combined);
        shapes.begin(ShapeRenderer.ShapeType.Filled);
        if (dragging && !model.getWaitForBall()) {
            float distance = ballPos.dst2(new Vector2(tp.x, tp.y));
            float normalized = Math.min(distance / 100, 1f);
            shapes.setColor(1f, 1f - normalized, 1f - normalized, 1f);
            shapes.rectLine(ballPos, new Vector2(tp.x, tp.y), Math.max(normalized, 0.2f));
            shapes.circle(tp.x, tp.y, 0.5f, 16);
        }

        Goal[] goals = model.getGoals();
        shapes.setColor(Color.GREEN);
        for (int i = 0; i < goals.length; i++){
            shapes.circle(goals[i].getPos().x, goals[i].getPos().y, goals[i].getRad(), 16);
        }


        shapes.setColor(Color.WHITE);
        batch.begin();
        font.getData().setScale(4f);
        font.draw(batch, String.format("%s's turn", model.getPlayerList().get(currentPlayer).getPlayerName()),
                Gdx.graphics.getWidth() / 10 - 120,
                Gdx.graphics.getHeight() - 20);
        font.draw(batch, String.format("Number of strokes: %s", model.getPlayerList().get(currentPlayer).getStrokes()),
                Gdx.graphics.getWidth() / 10 + 500,
                Gdx.graphics.getHeight() - 20);

        batch.end();
        for (int i : model.getActivePlayers()){
            if (i == currentPlayer && model.isActivePlayer()){
                shapes.setColor(Color.RED);

            }
            shapes.circle(model.getBallPos(i).x, model.getBallPos(i).y, model.getBallRad(i), 16);
            shapes.setColor(Color.WHITE);
        }
        shapes.end();
    }

    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    public void dispose(){
        shapes.dispose();
    }
}

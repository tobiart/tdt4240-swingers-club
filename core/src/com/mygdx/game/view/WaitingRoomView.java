package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.model.Menu;
import com.mygdx.game.model.Model;
import com.mygdx.game.model.Player;

import java.util.ArrayList;

public class WaitingRoomView implements Screen {
    private final Golf golf;
    private Stage stage;
    private Skin skin;
    private static WaitingRoomView INSTANCE;
    API api;
    Menu menu;
    private ShapeRenderer shapes;
    private Label outputLabel;
    private int sessionId;
    private boolean host;
    private ArrayList<Player> playerList;
    private Boolean hasStarted;

    private WaitingRoomView(API api, Stage stage, Golf golf) {
        this.api = api;
        this.menu = Menu.getInstance(api);
      //  this.shapes = new ShapeRenderer();
        this.stage = stage;
        this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.5f);
        this.golf = golf;
        hasStarted=false;
    }

    public void init(int sessionId){
        this.sessionId = sessionId;
        playerList = new ArrayList<>();
        api.getPlayers(sessionId,playerList);
        hasStarted = false;
    }

    @Override
    public void show() {

    }

    public static WaitingRoomView getInstance(API api, Stage stage, Golf golf){
        if (INSTANCE == null){
            INSTANCE = new WaitingRoomView(api, stage, golf);
        }
        return INSTANCE;
    }

    @Override
    public void render(float delta) {
        stage.clear();
        Gdx.gl.glClearColor(0,0,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        String labelText = "Players:\n\n";
        for(Player player : this.playerList){
            labelText = labelText + player.getPlayerName()+"\n";
        }
        final Label playerlist = new Label(labelText,skin);
        playerlist.setSize(Gdx.graphics.getWidth(),40);
        playerlist.setPosition(0,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2);
        playerlist.setAlignment(Align.center);
        stage.addActor(playerlist);

        if (host) {
            Button button2 = new TextButton("Start game", skin);
            button2.setSize(100 * 4, 100);
            button2.setPosition(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 2 - 200, Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 2 - 200);
            button2.addListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    Model model = Model.getInstance(api);
                    model.setPlayerList(playerList);
                    api.startSession(sessionId);
                    golf.changeToGame(sessionId, true);
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }
            });
            stage.addActor(button2);
        }
        if(hasStarted){
            Model model = Model.getInstance(api);
            model.setPlayerList(playerList);
            golf.changeToGame(sessionId, true);
        }
        outputLabel = new Label("Roomkey: "+ this.sessionId,skin);
        outputLabel.setSize(Gdx.graphics.getWidth(),40);
        outputLabel.setPosition(0,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2+200);
        outputLabel.setAlignment(Align.center);
        stage.addActor(outputLabel);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }


    @Override
    public void dispose() {
        shapes.dispose();
        stage.dispose();


    }

    @Override
    public void hide() {
        stage = null;
        skin = null;
    }

    public void setHost(boolean host) {
        this.host = host;

    }

    public void addListener(boolean hasStarted) {
this.hasStarted = hasStarted;
    }
}

package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Golf;

public class TutorialView implements Screen {
    private Skin skin;
    private Stage stage;
    private static TutorialView INSTANCE;

    private SpriteBatch batch;

    private Texture menu, createGame, joinGame, highscore, game1;
    private Array<Texture> frames;
    private int currentTextureIndex;

    private Golf golf;

    public TutorialView(Stage stage, Golf golf) {
        this.stage = stage;
        this.golf = golf;
        this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.5f);
        menu = new Texture("menu.png");
        createGame = new Texture("createGame.png");
        joinGame = new Texture("joinGame.png");
        highscore = new Texture("highscore.png");
        game1 = new Texture("game1.png");
        batch = new SpriteBatch();
        frames = new Array<Texture>();
        frames.add(menu);
        frames.add(createGame);
        frames.add(joinGame);
        frames.add(highscore);
        frames.add(game1);

        currentTextureIndex = 0;
    }

    public static TutorialView getInstance(Stage stage, Golf golf){
        if (INSTANCE == null){
            INSTANCE = new TutorialView(stage, golf);
        }
        return INSTANCE;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.clear();
        batch.begin();
        batch.draw(frames.get(currentTextureIndex), (Gdx.graphics.getWidth()/2)-(frames.get(currentTextureIndex).getWidth()/2), (Gdx.graphics.getHeight()/2)-(frames.get(currentTextureIndex).getHeight()/2), 0, 0, frames.get(currentTextureIndex).getWidth(), frames.get(currentTextureIndex).getHeight());
        batch.end();

        Button nextButton = new TextButton("Next", skin);
        nextButton.setSize(100 * 4,100);
        nextButton.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2+700,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2);
        nextButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                update(true);
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        if (!(currentTextureIndex >= frames.size-1)) {
            stage.addActor(nextButton);
        }

        Button prevButton = new TextButton("Previous", skin);
        prevButton.setSize(100 * 4,100);
        prevButton.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2-1100,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2);
        prevButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                update(false);
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        if (!(currentTextureIndex<=0)) {
            stage.addActor(prevButton);
        }

        Button menuButton = new TextButton("Back to menu", skin);
        menuButton.setSize(100 * 4, 100);
        menuButton.setPosition(200, Gdx.graphics.getHeight() - 200);

        menuButton.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                golf.changeToMenu();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(menuButton);

        stage.act();
        stage.draw();
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {
        stage = null;
        skin = null;
    }

    public void update(boolean next) {
        if (next && currentTextureIndex<frames.size) {
            currentTextureIndex++;

        } else if (next == false && currentTextureIndex>0) {
            currentTextureIndex--;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }


    @Override
    public void dispose() {
        stage.dispose();
    }
}

package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.model.Menu;

public class ErrorFeedBackView implements Screen {
    private static ErrorFeedBackView INSTANCE;
    private final Golf golf;
    private Stage stage;
    private Skin skin;
    private ShapeRenderer shapes;
    private Label outputLabel;




        public ErrorFeedBackView(Golf golf, Stage stage) {
            this.stage = stage;
            this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
            skin.getFont("default-font").getData().setScale(2.5f);
            this.golf = golf;
        }

        public static ErrorFeedBackView getInstance(Golf golf, Stage stage){
            if (INSTANCE == null){
                INSTANCE = new ErrorFeedBackView(golf, stage);
            }
            return INSTANCE;
        }

        @Override
        public void show() {

        }

        @Override
        public void hide() {
            stage = null;
            skin = null;
        }

        @Override
        public void render(float delta) {
            Gdx.gl.glClearColor(0,0,0.2f,1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            Button button = new TextButton("Back to menu", skin);
            button.setSize(100 * 4, 100);
            button.setPosition(Gdx.graphics.getWidth() / 2 - 200, Gdx.graphics.getHeight() / 10);

            button.addListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    golf.changeToMenu();
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }
            });
            stage.addActor(button);

            outputLabel = new Label("That room has already started, Create another one!",skin);
            outputLabel.setSize(Gdx.graphics.getWidth(),40);
            outputLabel.setPosition(0,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2+100);
            outputLabel.setAlignment(Align.center);

            stage.addActor(outputLabel);


            stage.act();
            stage.draw();
        }

        @Override
        public void resize(int width, int height) {

        }

        @Override
        public void pause() {

        }

        @Override
        public void resume() {

        }

        @Override
        public void dispose() {
            shapes.dispose();
            stage.dispose();


        }}


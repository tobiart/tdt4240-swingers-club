package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.Score;
import com.mygdx.game.model.Scoreboard;

import java.util.ArrayList;

public class LeaderboardView implements Screen {

    private static LeaderboardView INSTANCE;
    private API api;
    private Stage stage;
    private Golf golf;
    private ShapeRenderer shapes;
    private Skin skin;
    private Scoreboard leaderboard;
    boolean isMidgame;
    boolean isHighscoreUpdater;

    private LeaderboardView(API api, Stage stage, Golf golf){
        this.api = api;
        this.stage = stage;

        this.golf = golf;
        this.shapes = new ShapeRenderer();
        this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.5f);
        this.leaderboard = new Scoreboard();
    }

    public static LeaderboardView getInstance(API api, Stage stage, Golf golf){
        if (INSTANCE == null){
            INSTANCE = new LeaderboardView(api, stage, golf);
        }
        return INSTANCE;
    }

    public void setHighscoreUpdater(boolean highscoreUpdater) {
        isHighscoreUpdater = highscoreUpdater;
    }

    @Override
    public void render(float delta){
        stage.clear();
        Gdx.gl.glClearColor(0,0,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Label leaderboardLabel = new Label("Leaderboard",skin);
        leaderboardLabel.setSize(Gdx.graphics.getWidth(),40);
        leaderboardLabel.setPosition(0,Gdx.graphics.getHeight()/4*3);
        leaderboardLabel.setAlignment(Align.center);
        stage.addActor(leaderboardLabel);

        if ((!leaderboard.getScoreList("course1").isEmpty()) && leaderboard.getScoreList("total").isEmpty()){
            leaderboard.sortByName();
            leaderboard.calculateTotal();
            leaderboard.sortByName();
            if (isHighscoreUpdater && !isMidgame){
                System.out.println("Kommer inn i leaderboardView if isHighscoreUpdater");
                leaderboard.updateDatabaseHighscore(api);
            }
        }

        Table table = new Table();
        table.add(new Label("-", skin)).expandX();
        table.add(new Label("Hole 1:", skin)).expandX();
        table.add(new Label("Hole 2:", skin)).expandX();
        table.add(new Label("Hole 3:", skin)).expandX();
        table.add(new Label("Hole 4:", skin)).expandX();
        table.add(new Label("Total:", skin)).expandX();

        for (int row = 0; row < leaderboard.getScoreLength(); row++){
            table.row();
            table.add(new Label(leaderboard.getScoreList("course1").get(row).getPlayer(), skin));
            try {
                table.add(new Label(leaderboard.getScoreList("course1").get(row).getScore()+"", skin));
            } catch (IndexOutOfBoundsException e){
                table.add(new Label("-", skin));
            }

            try {
                table.add(new Label(leaderboard.getScoreList("course2").get(row).getScore()+"", skin));
            } catch (IndexOutOfBoundsException e){
                table.add(new Label("-", skin));
            }

            try {
                table.add(new Label(leaderboard.getScoreList("course3").get(row).getScore()+"", skin));
            } catch (IndexOutOfBoundsException e){
                table.add(new Label("-", skin));
            }

            try {
                table.add(new Label(leaderboard.getScoreList("course4").get(row).getScore()+"", skin));
            } catch (IndexOutOfBoundsException e){
                table.add(new Label("-", skin));
            }

            try {
                table.add(new Label(leaderboard.getScoreList("total").get(row).getScore()+"", skin));
            } catch (IndexOutOfBoundsException e){
                table.add(new Label("-", skin));
            }
        }

        table.setPosition(0,Gdx.graphics.getHeight() / 2);
        table.setSize(Gdx.graphics.getWidth(), 300);
        stage.addActor(table);


        TextButton button = new TextButton("",skin);
        button.setSize(100 * 4, 100);
        button.setPosition(Gdx.graphics.getWidth() / 2 - 200, Gdx.graphics.getHeight() / 10);
        if (isMidgame){
            button.setText("Return to game");
            button.addListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    golf.changeToGame(golf.getSessionId(), false);
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }
            });
        } else {
            button.setText("Return to menu");
            button.addListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    golf.changeToMenu();
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }
            });

        }


        stage.addActor(button);
        stage.act();
        stage.draw();

    }

    public void fetchScores(int sessionId){
        leaderboard.clear();
        api.getScores(sessionId, leaderboard);
    }

    public void setIsMidgame(boolean setting){
        this.isMidgame = setting;
    }


    @Override
    public void show() {
    }

    @Override
    public void hide() {
        stage = null;
        skin = null;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        shapes.dispose();
        stage.dispose();


    }
}

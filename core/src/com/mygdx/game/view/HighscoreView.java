package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.Score;
import com.mygdx.game.model.Player;
import com.mygdx.game.model.Scoreboard;

import java.util.ArrayList;

public class HighscoreView implements Screen {

    private static HighscoreView INSTANCE;
    API api;
    Stage stage;
    Golf golf;
    ArrayList<Score> highscoreList;
    Scoreboard highscores;
    ShapeRenderer shapes;
    Skin skin;


    private HighscoreView(API api, Stage stage, Golf golf) {
        this.api = api;
        this.stage = stage;
        this.golf = golf;
        this.shapes = new ShapeRenderer();
        this.highscoreList = new ArrayList<Score>();
        this.highscores = new Scoreboard();
        this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.5f);
    }

    public static HighscoreView getInstance(API api, Stage stage, Golf golf){
        if (INSTANCE == null){
            INSTANCE = new HighscoreView(api, stage, golf);
        }
        INSTANCE.fetchHighscore();
        return INSTANCE;
    }
    private void fetchHighscore() {
        highscores.clear();
        api.getHighscores(highscores);
    }

    public void createElements(){

    }

    @Override
    public void render(float delta) {
        stage.clear();
        Gdx.gl.glClearColor(0,0,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Table table = new Table();
        table.add(new Label("Hole 1:", skin)).expandX();
        table.add(new Label("Hole 2:", skin)).expandX();
        table.add(new Label("Hole 3:", skin)).expandX();
        table.add(new Label("Hole 4:", skin)).expandX();
        table.add(new Label("Total:", skin)).expandX();

        for (int row = 0; row < highscores.getScoreLength(); row++){
            table.row();
            Label course1Score = new Label(highscores.getScoreList("course1").get(row).toString(), skin);
            table.add(course1Score);
            Label course2Score = new Label(highscores.getScoreList("course2").get(row).toString(), skin);
            table.add(course2Score);
            Label course3Score = new Label(highscores.getScoreList("course3").get(row).toString(), skin);
            table.add(course3Score);
            Label course4Score = new Label(highscores.getScoreList("course4").get(row).toString(), skin);
            table.add(course4Score);
            Label totalScore = new Label(highscores.getScoreList("total").get(row).toString(), skin);
            table.add(totalScore);
        }
        table.setPosition(0,Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 2 - 200);
        table.setSize(Gdx.graphics.getWidth(), 300);
        stage.addActor(table);

        //ScrollPane scrollPane = new ScrollPane(table, skin);
        //scrollPane.setPosition(0,Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 2 - 200);
        //scrollPane.setSize(Gdx.graphics.getWidth(), 300);
        //scrollPane.setHeight(500);

        //Table containerTable = new Table();
        //containerTable.add(scrollPane).height(100);
        //containerTable.setPosition(0,Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 2 - 200);
        //containerTable.setWidth(Gdx.graphics.getWidth());
        //stage.addActor(containerTable);

        Label highscoreLabel = new Label("Highscores:\n\n",skin);
        highscoreLabel.setSize(Gdx.graphics.getWidth(),40);
        highscoreLabel.setPosition(0,Gdx.graphics.getHeight()/4*3);
        highscoreLabel.setAlignment(Align.center);
        stage.addActor(highscoreLabel);

        Button button = new TextButton("Back to menu", skin);
        button.setSize(100 * 4, 100);
        button.setPosition(Gdx.graphics.getWidth() / 2 - 200, Gdx.graphics.getHeight() / 10);

        button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                golf.changeToMenu();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(button);

        stage.act();
        stage.draw();
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
        stage = null;
        skin = null;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        shapes.dispose();
        stage.dispose();


    }



}

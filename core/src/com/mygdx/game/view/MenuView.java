package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.API;
import com.mygdx.game.Golf;
import com.mygdx.game.model.Menu;

import java.util.Random;

public class MenuView implements Screen {
    private Stage stage;
    private Skin skin;
    private static MenuView INSTANCE;
    private API api;
    private Menu menu;
    private ShapeRenderer shapes;
    private Label outputLabel;
    private TextField usernameTextField;




    public MenuView(final API api, Stage stage) {
        this.api = api;
        this.menu = Menu.getInstance(api);
        this.shapes = new ShapeRenderer();
        this.stage = stage;
        this.skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.5f);
    }

    public static MenuView getInstance(API api, Stage stage){
        if (INSTANCE == null){
            INSTANCE = new MenuView(api, stage);
        }
        return INSTANCE;
    }

    public void createElements(final Golf golf){
        Button button2 = new TextButton("Create room",skin);
        button2.setSize(100*4,100);
        button2.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2-500,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2-200);
        button2.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                outputLabel.setText("Hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
                System.out.println(usernameTextField.getText());
                Random r = new Random();
                int sessionId = r.nextInt(10000);
                api.createSession(sessionId, usernameTextField.getText());
                System.out.println(sessionId);
                golf.changeToWaitingRoom(sessionId,usernameTextField.getText(), true);

            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                outputLabel.setText("Pressed Text Button");
                return true;
            }
        });
        stage.addActor(button2);
        Button button1 = new TextButton("Join room",skin);
        button1.setSize(100*4,100);
        button1.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2+100,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2-200);
        button1.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                golf.joinRoom(usernameTextField.getText());

            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                outputLabel.setText("Pressed Text Button");
                return true;
            }
        });
        stage.addActor(button1);
        Button button3 = new TextButton("Highscores",skin);
        button3.setSize(100*4,100);
        button3.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2-500,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2-400);
        button3.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                golf.changeToHighscores();

            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(button3);

        Button button4 = new TextButton("Tutorial", skin);
        button4.setSize(100*4,100);
        button4.setPosition(Gdx.graphics.getWidth()-Gdx.graphics.getWidth()/2+100,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2-400);
        button4.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                golf.goToTutorial();

            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                outputLabel.setText("Pressed Text Button");
                return true;
            }
        });
        stage.addActor(button4);

        outputLabel = new Label("Enter your name to start!",skin);
        outputLabel.setSize(Gdx.graphics.getWidth(),40);
        outputLabel.setPosition(0,Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2+300);
        outputLabel.setAlignment(Align.center);
        usernameTextField = new TextField("Navn her", skin);
        usernameTextField.setPosition(Gdx.graphics.getWidth()-(Gdx.graphics.getWidth()/2+230),Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/2+100);
        usernameTextField.setSize(500, 100);

        stage.addActor(usernameTextField);
        stage.addActor(outputLabel);

    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {
        stage = null;
        skin = null;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        shapes.dispose();
        stage.dispose();


    }}

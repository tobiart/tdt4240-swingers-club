package com.mygdx.game;

import com.mygdx.game.model.Courses;
import com.mygdx.game.model.Model;
import com.mygdx.game.model.Player;
import com.mygdx.game.model.Scoreboard;
import com.mygdx.game.view.WaitingRoomView;

import java.util.ArrayList;

public interface API {
    void submitHighscore(Scoreboard score);
    void getHighscores(Scoreboard dataHolder);
    void getScores(int sessionId, Scoreboard dataHolder);
    void hitGoal(int sessionId, Player player, int playerNr);
    void watchBalls(int sessionId,ArrayList<Player> playerList, boolean addListener);
    void sendBallData(int sessionId, ArrayList<Player> players);
    void createSession(int sessionId, String player);
    boolean joinSession(int sessionId, String player, Golf golf);
    void unsetFinished(int sessionId, String player);
    void isFinishedListener(int sessionId, ArrayList<Player> playerFinishedStatus);

    void startSession(int sessionId);
    void getPlayers(int sessionId,ArrayList<Player> dataHolder);
    void waitForGameStart(int sessionId, ArrayList<Boolean> dataHolder, WaitingRoomView hasStarted);
    void addStroke(int sessionId,Player player);
    void setCurrentPlayer(int sessionId, int player);
    void currentPlayerListener(int sessionId, ArrayList<Integer> dataHolder);
    void currentCourseListener(int sessionId, ArrayList<Integer> dataHolder);
    void setCurrentCourse(int sessionId, int course);

    void sendTeleportedBall(int sessionId, Player player);
}
